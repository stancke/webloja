from venda.models import *
from django.contrib import admin
from cadastro.models import Cliente, Estoque
from venda.models import Venda, CarrinhoDeCompra

class CarrinhoDeCompraInline(admin.StackedInline):
    model = CarrinhoDeCompra

class VendaAdmin(admin.ModelAdmin):
    
    inlines = [
        CarrinhoDeCompraInline,
    ]

admin.site.register(Venda, VendaAdmin)


