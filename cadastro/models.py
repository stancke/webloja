from django.db import models

class Cliente(models.Model):
    
    nome = models.CharField(max_length = 20)
    sobrenome = models.CharField(max_length = 100)
    data_de_nascimento = models.DateField(null=True, blank=True)
    rg = models.IntegerField(max_length=8, null=True, blank=True)
    cpf = models.IntegerField(max_length=11, null=True, blank=True)
    logradouro = models.CharField(max_length = 100, null=True, blank=True)
    telefone = models.CharField(max_length = 100, null=True)    
    celular = models.CharField(max_length = 100, null=True, blank=True)
    e_mail = models.EmailField(max_length= 109, null=True, blank=True)
    
    def __unicode__(self):
        return self.nome

class Fornecedor(models.Model):
    
    nome = models.CharField(max_length = 20)
    cnpj = models.IntegerField(max_length=100, null=True, blank=True)   
    endereco = models.CharField(max_length = 100, null=True, blank=True)
    logradouro = models.CharField(max_length = 100, null=True, blank=True)
    telefone = models.IntegerField(max_length = 100, null=True)
    celular = models.IntegerField(max_length=100, null=True, blank=True)
    e_mail = models.EmailField(max_length= 109, null=True, blank=True)
    site = models.CharField(max_length= 109, null=True, blank=True)
    
    def __unicode__(self):
        return self.nome

class Estoque(models.Model):
    
    fornecedor = models.ForeignKey(Fornecedor, null=True, blank=True)
    nome = models.CharField(max_length=100) 
    modelo = models.CharField(max_length=100, null=True, blank=True)
    cor = models.CharField(max_length=50, null=True, blank=True)
    quantidade = models.IntegerField(max_length=50)
    preco_de_compra = models.DecimalField(max_digits=5, decimal_places=2, blank=True)
    preco_de_venda = models.DecimalField(max_digits=5, decimal_places=2)
    #foto_do_produto = models.ImageField(upload_to="images")
    
    def __unicode__(self):
        return self.nome

