from django.db import models
from cadastro.models import Cliente, Estoque

class Venda(models.Model):

    cliente = models.ForeignKey(Cliente)

class CarrinhoDeCompra(models.Model):

    venda = models.ForeignKey(Venda)
    produto = models.ForeignKey(Estoque)
    quantidade = models.IntegerField(max_length=10, null=True, blank=True)
